# Flutter :: I_am_start

Первое мобильное приложение под Android, разработанное посредством [flutter](https://flutter.dev).  
Данное приложение это аналог приложения [I_Am_Rich](https://ru.wikipedia.org/wiki/I_Am_Rich).  
[Скачать APK](https://gitlab.com/5w/flutter_i_am_start/-/blob/main/build/app/outputs/apk/release/app-release.apk)

## Скриншоты
![](https://gitlab.com/5w/flutter_i_am_start/-/raw/main/readme_assets/Screenshot_20240702_130146.png)
![](https://gitlab.com/5w/flutter_i_am_start/-/raw/main/readme_assets/Screenshot_20240702_130243.png)


## Стек технологий
Flutter 3.22


## Справочная информация
- [Лаборатория: напишите свое первое приложение Flutter](https://docs.flutter.dev/get-started/codelab)
- [Примеры](https://docs.flutter.dev/cookbook)
- [Документация](https://docs.flutter.dev/)


## Контакты
[Отправить письмо](mailto:derecov+flutter_i_am_start@gmail.com)


## Лицензия
[MIT](https://choosealicense.com/licenses/mit/)
