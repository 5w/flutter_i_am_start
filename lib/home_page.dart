import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _showImage = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('I am start'),
        centerTitle: true,
        titleTextStyle: const TextStyle(color: Colors.white),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              _showImage = !_showImage;
            });
          },
          child: _showImage
              ? const Image(
                  image: AssetImage('images/flutter-logo.png'),
                )
              : const Text(
                  'Hello world',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                  ),
                ),
        ),
      ),
    );
  }
}
